<?php

use Illuminate\Database\Seeder;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Database\Eloquent\Model;

class TenantDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $groups = Sentinel::getRoleRepository();

        // Create an Admin group
        $groups->createModel()->create(
            [
                'name' => 'Admin',
                'slug' => 'admin',
            ]
        );

        // Create an Users group
        $groups->createModel()->create(
            [
                'name' => 'User',
                'slug' => 'user',
            ]
        );

        // Save the permissions
        $group = Sentinel::findRoleBySlug('admin');
        $group->permissions = [
            'core.sidebar.group' => true,
            /* Dashboard */
            'dashboard.index' => true,
            'dashboard.update' => true,
            'dashboard.reset' => true,
            /* Workbench */
            'workshop.sidebar.group' => true,
            'workshop.modules.index' => true,
            'workshop.modules.show' => true,
            'workshop.modules.update' => true,
            'workshop.modules.disable' => true,
            'workshop.modules.enable' => true,
            'workshop.modules.publish' => true,
            'workshop.themes.index' => true,
            'workshop.themes.show' => true,
            'workshop.themes.publish' => true,
            /* Roles */
            'user.roles.index' => true,
            'user.roles.create' => true,
            'user.roles.edit' => true,
            'user.roles.destroy' => true,
            /* Users */
            'user.users.index' => true,
            'user.users.create' => true,
            'user.users.edit' => true,
            'user.users.destroy' => true,
            /* API keys */
            'account.api-keys.index' => true,
            'account.api-keys.create' => true,
            'account.api-keys.destroy' => true,
            /* Menu */
            'menu.menus.index' => true,
            'menu.menus.create' => true,
            'menu.menus.edit' => true,
            'menu.menus.destroy' => true,
            'menu.menuitems.index' => true,
            'menu.menuitems.create' => true,
            'menu.menuitems.edit' => true,
            'menu.menuitems.destroy' => true,
            /* Media */
            'media.medias.index' => true,
            'media.medias.create' => true,
            'media.medias.edit' => true,
            'media.medias.destroy' => true,
            'media.folders.index' => true,
            'media.folders.create' => true,
            'media.folders.edit' => true,
            'media.folders.destroy' => true,
            /* Settings */
            'setting.settings.index' => true,
            'setting.settings.edit' => true,
            /* Page */
            'page.pages.index' => true,
            'page.pages.create' => true,
            'page.pages.edit' => true,
            'page.pages.destroy' => true,
            /* Translation */
            'translation.translations.index' => true,
            'translation.translations.edit' => true,
            'translation.translations.export' => true,
            'translation.translations.import' => true,
            /* Tags */
            'tag.tags.index' => true,
            'tag.tags.create' => true,
            'tag.tags.edit' => true,
            'tag.tags.destroy' => true,
        ];
        $excludePermissions=env('EXCLUDE_PERMISSIONS', false);
        if ($excludePermissions){
            $group->permissions=[
                "backup.backup.index"=>true,
                "backup.backup.create"=>true,
                "backup.backup.delete"=>true,
                "benfeciaries.recipients.index"=>true,
                "benfeciaries.recipients.create"=>true,
                "benfeciaries.recipients.edit"=>true,
                "benfeciaries.recipients.show"=>true,
                "benfeciaries.recipients.destroy"=>true,
                "benfeciaries.recipients.uploadMedia"=>true,
                "benfeciaries.recipients.showMedia"=>true,
                "benfeciaries.recipients.downloadMedia"=>true,
                "benfeciaries.recipients.deleteMedia"=>true,
                "benfeciaries.delegates.index"=>true,
                "benfeciaries.delegates.create"=>true,
                "benfeciaries.delegates.edit"=>true,
                "benfeciaries.delegates.show"=>true,
                "benfeciaries.delegates.destroy"=>true,
                "benfeciaries.delegates.uploadMedia"=>true,
                "benfeciaries.delegates.showMedia"=>true,
                "benfeciaries.delegates.downloadMedia"=>true,
                "benfeciaries.delegates.deleteMedia"=>true,
                "benfeciaries.persons.index"=>true,
                "benfeciaries.persons.create"=>true,
                "benfeciaries.persons.edit"=>true,
                "benfeciaries.persons.show"=>true,
                "benfeciaries.persons.destroy"=>true,
                "benfeciaries.persons.uploadMedia"=>true,
                "benfeciaries.persons.showMedia"=>true,
                "benfeciaries.persons.downloadMedia"=>true,
                "benfeciaries.persons.deleteMedia"=>true,
                "department.departments.index"=>true,
                "department.departments.create"=>true,
                "department.departments.edit"=>true,
                "department.departments.destroy"=>true,
                "department.actions.userControl"=>true,
                "employee.employees.index"=>true,
                "employee.employees.create"=>true,
                "employee.employees.edit"=>true,
                "employee.employees.destroy"=>true,
                "employee.employees.restore"=>true,
                "employee.employees.profile"=>true,
                "employee.employees.uploadMedia"=>true,
                "employee.employees.profileUploadMedia"=>true,
                "employee.employees.showMedia"=>true,
                "employee.employees.downloadMedia"=>true,
                "employee.employees.deleteMedia"=>true,
                "folder.folders.index"=>true,
                "folder.folders.create"=>true,
                "folder.folders.edit"=>true,
                "folder.folders.destroy"=>true,
                "folder.folders.show"=>true,
                "messages.actions.new"=>true,
                "messages.actions.inbox"=>true,
                "messages.actions.outbox"=>true,
                "messages.actions.reply"=>true,
                "message.msgs.uploadMedia"=>true,
                "message.msgs.showMedia"=>true,
                "message.msgs.downloadMedia"=>true,
                "message.msgs.deleteMedia"=>true,
                "setting.settings.index"=>true,
                "setting.settings.edit"=>true,
                "tag.tags.index"=>true,
                "tag.tags.create"=>true,
                "tag.tags.edit"=>true,
                "tag.tags.destroy"=>true,
                "user.users.index"=>true,
                "user.users.create"=>true,
                "user.users.edit"=>true,
                "user.users.destroy"=>true,
                "user.roles.index"=>true,
                "user.roles.create"=>true,
                "user.roles.edit"=>true,
                "user.roles.destroy"=>true
            ];
        }
        $group->save();
    }
}
