<?php

namespace Modules\Dashboard\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Modules\Benfeciaries\Entities\Delegates;
use Modules\Benfeciaries\Entities\Persons;
use Modules\Benfeciaries\Entities\Recipients;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Modules\Dashboard\Repositories\WidgetRepository;
use Modules\Tasking\Entities\Task;
use Modules\User\Contracts\Authentication;
use Modules\User\Entities\Sentinel\User;
use Nwidart\Modules\Contracts\RepositoryInterface;

class DashboardController extends AdminBaseController
{
    /**
     * @var WidgetRepository
     */
    private $widget;
    /**
     * @var Authentication
     */
    private $auth;

    /**
     * @param RepositoryInterface $modules
     * @param WidgetRepository $widget
     * @param Authentication $auth
     */
    public function __construct(RepositoryInterface $modules, WidgetRepository $widget, Authentication $auth)
    {
        parent::__construct();
        $this->bootWidgets($modules);
        $this->widget = $widget;
        $this->auth = $auth;
    }

    /**
     * Display the dashboard with its widgets
     * @return \Illuminate\View\View
     */
    public function index()
    {

        $widget = $this->widget->findForUser($this->auth->id());

        $customWidgets = json_encode(null);
        if ($widget) {
            $customWidgets = $widget->widgets;
        }

        return view('dashboard::admin.dashboard', compact('customWidgets'));
    }

    /**
     * Save the current state of the widgets
     * @param Request $request
     * @return mixed
     */
    public function save(Request $request)
    {
        $widgets = $request->get('grid');

        if (empty($widgets)) {
            return Response::json([false]);
        }

        $this->widget->updateOrCreateForUser($widgets, $this->auth->id());

        return Response::json([true]);
    }

    /**
     * Reset the grid for the current user
     */
    public function reset()
    {
        $widget = $this->widget->findForUser($this->auth->id());

        if (!$widget) {
            return redirect()->route('dashboard.index')->with('warning', trans('dashboard::dashboard.reset not needed'));
        }

        $this->widget->destroy($widget);

        return redirect()->route('dashboard.index')->with('success', trans('dashboard::dashboard.dashboard reset'));
    }

    /**
     * Boot widgets for all enabled modules
     * @param RepositoryInterface $modules
     */
    private function bootWidgets(RepositoryInterface $modules)
    {
        foreach ($modules->enabled() as $module) {
            if (! $module->widgets) {
                continue;
            }
            foreach ($module->widgets as $widgetClass) {
                app($widgetClass)->boot();
            }
        }
    }





    public function browserScan() {
        return view('partials.browser-scanner');
    }
    public function imageEditor() {
        return view('partials.scan-image-editor');
    }

    public function imageEditorUpload(Request $request){
        $file=$request->croppedImage;
        $id=$request->moduleID;
        if($request->moduleName=='employee'){
            $user=User::find($id);
            $route='admin.employee.employee.updateprofile';
            $this->addImageToDisk($user,'archive',$file);
        }
        else if ($request->moduleName=='task'){
            $task=Task::find($id);
            $route='admin.tasking.task.edit';
            $this->addImageToDisk($task,'task',$file);
        }
         else if ($request->moduleName=='recipient'){
            $recipient=Recipients::find($id);
            $route='admin.benfeciaries.recipients.edit';
            $this->addImageToDisk($recipient,'recipient',$file);
        }
         else if ($request->moduleName=='delegates'){
             $delegets=Delegates::find($id);
             $route='admin.benfeciaries.delegates.edit';
             $this->addImageToDisk($delegets,'delegates',$file);
         }
         else if ($request->moduleName=='person'){
             $person=Persons::find($id);
             $route='benfeciaries::admin.persons.edit';
             $this->addImageToDisk($person,'person',$file);
         }
         else if ($request->moduleName=='transaction'){
             if (is_module_enabled('Importexport')){
                 $user=\Modules\Importexport\Entities\Transaction::find($id);
                 $route='admin.importexport.transaction.edit';
                 $this->addImageToDisk($user,'transaction',$file);
             }
         }
        return redirect()->route($route,$id)->withSuccess(trans('core::core.scan.success'));


    }

    private function addImageToDisk($model,$diskName,$file){
        $model->addMediaFromBase64($file)
            ->usingFileName(md5(time() . rand()) . '.' . 'PNG')
            ->toMediaCollection($diskName, 'media-disk');
    }
}
