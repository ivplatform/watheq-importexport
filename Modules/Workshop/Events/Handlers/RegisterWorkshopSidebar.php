<?php

namespace Modules\Workshop\Events\Handlers;

use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\Core\Sidebar\AbstractAdminSidebar;

class RegisterWorkshopSidebar extends AbstractAdminSidebar
{
    /**
     * Method used to define your sidebar menu groups and items
     * @param Menu $menu
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('setting::settings.title.settings'), function (Item $item) {
                $item->item(trans('workshop::workshop.modules'), function (Item $item) {
                    $item->icon('fa fa-cogs');
                    $item->weight(7);
                    $item->route('admin.workshop.modules.index');
                    $item->authorize(
                        $this->auth->hasAccess('workshop.modules.index')
                    );
                });
                $item->item(trans('workshop::workshop.themes'), function (Item $item) {
                    $item->icon('fa fa-cogs');
                    $item->weight(8);
                    $item->route('admin.workshop.themes.index');
                    $item->authorize(
                        $this->auth->hasAccess('workshop.themes.index')
                    );
                });
            });
        });
        return $menu;
    }
}
