<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\Tasking\Entities\Task;
use Carbon\Carbon;
use Modules\Notify\Contract\Notification;

class TaskEndDateOver extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'endDateOver:task';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send Notification for user when  end date of task ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $notify;

    public function __construct(Notification $notify) {
        parent::__construct();
        $this->notify = $notify;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        // calculate new statistics
        $sendToUser = [];

//        $tasks = Task::where('endtime', '>=', Carbon::today()->addDays(0))
//            ->get();
        $tasks = Task::where('endtime', '>=', Carbon::today()->addDays(0))
            ->where('endtime', '<', Carbon::today()->addDays(1))
            ->get();
       // \Log::info('end date: '.$tasks->endtime. ' date tomorrow:'. Carbon::today());;

        // update statistics table
        // send notification for each user that acrhive has assigned to him/her
        foreach ($tasks as $task) {
            /////////////////////////////////////////////////
            // send notification for each user has been assign to acchive to tell expire date
            $sendToUser = [$task->user_id,$task->assign_to];
            //\Log::info('end date: '.$task->endtime. ' date tomorrow:'. Carbon::today());;
            /////////////////////////////////////////////////

            $jsonBody=json_encode(['number'=>$task->id,'end_date'=>$task->endtime]);

            $this->notify->push(
                "endDateOver_title",
                "endDateOver_body$#$$jsonBody",
                'fas fa-tasks',
                $sendToUser,
                'dashboard/tasking/show/'.$task->id, //route('admin.tasking.task.show', [$task->id])
                'tasking'
            );


            /////////////////////////////////////////////////
        }

    }

}
