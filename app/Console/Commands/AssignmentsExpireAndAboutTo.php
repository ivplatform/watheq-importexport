<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Modules\Importexport\Entities\Assignment;
use Modules\Importexport\Entities\InAssignment;
use Modules\Importexport\Entities\OutAssignment;
use Modules\Notify\Contract\Notification;

class AssignmentsExpireAndAboutTo extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'assignment:expire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'send Notification for user about Assignment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    protected $notify;

    public function __construct(Notification $notify) {
        parent::__construct();
        $this->notify = $notify;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->notifyAboutToExpireUsers();
        $this->notifyJustExpiredUsers();
    }
    private function notifyAboutToExpireUsers()
    {
        $deadline_notification=\Setting::get('importexport::deadline_notification');
        $hours=floor($deadline_notification);
        $min=($deadline_notification-$hours)*60;
        $deadTimeStartsFrom=Carbon::now()->addHours($hours)->addMinutes($min)->subSeconds(30);
        $deadTimeStartsTo=Carbon::now()->addHours($hours)->addMinutes($min)->addSeconds(30);
        $aboutToExpireOutAssignments=OutAssignment::where('admin_approved',0)->with('assignment')->whereHas('assignment',function ($query) use ($deadTimeStartsFrom,$deadTimeStartsTo){
            $query->where('milady_date','>',$deadTimeStartsFrom)->where('milady_date','<',$deadTimeStartsTo)->doesnthave('children') ;
        })->get();
        $aboutToExpireInAssignments=InAssignment::where('admin_action','!=',3)->with('assignment')->whereHas('assignment',function ($query) use ($deadTimeStartsFrom,$deadTimeStartsTo){
            dump($deadTimeStartsFrom);
            dump($deadTimeStartsTo);
            $query->where('milady_date','>',$deadTimeStartsFrom)->where('milady_date','<',$deadTimeStartsTo);
        })->get();
        $this->notifyUsersForOutAssignments($aboutToExpireOutAssignments,"about_to_expire");
        $this->notifyUsersForInAssignments($aboutToExpireInAssignments,"about_to_expire");
    }
    private function notifyJustExpiredUsers()
    {
        $deadTimeStartsFrom=Carbon::now()->subSeconds(30);
        $deadTimeStartsTo=Carbon::now()->addSeconds(30);
        $aboutToExpireOutAssignments=OutAssignment::where('admin_approved',0)->with('assignment')->whereHas('assignment',function ($query) use ($deadTimeStartsFrom,$deadTimeStartsTo){
            $query->where('milady_date','>',$deadTimeStartsFrom)->where('milady_date','<',$deadTimeStartsTo)->doesnthave('children') ;
        })->get();
        $aboutToExpireInAssignments=InAssignment::where('admin_action','!=',3)->with('assignment')->whereHas('assignment',function ($query) use ($deadTimeStartsFrom,$deadTimeStartsTo){
            $query->where('milady_date','>',$deadTimeStartsFrom)->where('milady_date','<',$deadTimeStartsTo)->doesnthave('children') ;
        })->get();
        $this->notifyUsersForOutAssignments($aboutToExpireOutAssignments,"expired");
        $this->notifyUsersForInAssignments($aboutToExpireInAssignments,"expired");
    }
    private function notifyUsersForOutAssignments(Collection $outAssignments, $type)
    {
        foreach ($outAssignments as $outAssignment)
        {
            $admin_id=$outAssignment->assignment->department->responsible->id;
            $assigner_id=$outAssignment->from_user;
            $this->notifyThoseUsers($type,[$admin_id,$assigner_id],['number'=>$outAssignment->assignment->id,'end_date'=>$outAssignment->assignment->milady_date]);
        }
    }
    private function notifyUsersForInAssignments(Collection $inAssignments, $type)
    {
        foreach ($inAssignments as $inAssignment){
            if (!$inAssignment->archived){
                if ($inAssignment->admin_action==0){
                    $admin_id=$inAssignment->assignment->department->responsible->id;
                    if ($inAssignment->assignment->parent){
                        $parent_admin_id=$inAssignment->assignment->parent->department->responsible->id;
                    }
                    else {
                        $parent_admin_id=null;
                    }
                    $this->notifyThoseUsers($type,[$admin_id,$parent_admin_id],['number'=>$inAssignment->assignment->id,'end_date'=>$inAssignment->assignment->milady_date]);
                }
                elseif ($inAssignment->admin_action==1){
                    //admin accepted but made nothing
                    $admin_id=$inAssignment->assignment->department->responsible->id;
                    if ($inAssignment->assignment->parent){
                        $parent_admin_id=$inAssignment->assignment->parent->department->responsible->id;
                    }
                    else {
                        $parent_admin_id=null;
                    }
                    $this->notifyThoseUsers($type,[$admin_id,$parent_admin_id],['number'=>$inAssignment->assignment->id,'end_date'=>$inAssignment->assignment->milady_date]);
                }
                elseif ($inAssignment->admin_action==2&&$inAssignment->chooser_id==null){
                    //admin assigned it but no one took it
                    $admin_id=$inAssignment->assignment->department->responsible->id;
                    $assignedToUsers=$inAssignment->users->pluck('id')->toArray();
                    array_push($assignedToUsers,$admin_id);
                    $this->notifyThoseUsers($type,$assignedToUsers,['number'=>$inAssignment->assignment->id,'end_date'=>$inAssignment->assignment->milady_date]);
                }
                elseif ($inAssignment->admin_action==2&&$inAssignment->chooser_id!=null){
                    //admin assigned and user took it
                    $admin_id=$inAssignment->assignment->department->responsible->id;
                    $chooser_id=$inAssignment->chooser_id;
                    $this->notifyThoseUsers($type,[$admin_id,$chooser_id],['number'=>$inAssignment->assignment->id]);
                }
            }
        }
        //all In
    }
    private function notifyThoseUsers($type,array $ids,array $data=null)
    {
        if ($type=="about_to_expire"){

            $jsonBody=json_encode($data );
            $title="assignment_is_about_to_end_title";
            $message="assignment_is_about_to_end_body$#$$jsonBody" ;

//            $title="Assignment Is About To End";
//            $message="You Have An Assignment That Is About To End";
            $icon="fa fa-copy";
        }
        elseif ($type=="expired"){
            $jsonBody=json_encode($data );
            $title="assignment_expired_title";
            $message="assignment_expired_body$#$$jsonBody" ;

//            $title="Assignment Expired";
//            $message="You Have An Assignment That Is Expired";
            $icon="fa fa-copy";
        }
        else {
            $title="";
            $message="";
            $icon="";
        }
        $link="dashboard/importexport/assignment/".$data['number'];
        $this->notify->push($title, $message, $icon,$ids, $link,'import_export',$emailableClass=null,$pushTo=null);
    }

}
