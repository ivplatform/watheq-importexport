<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Modules\Notify\Contract\Notification;
use Modules\Setting\Entities\Setting;
use Modules\User\Repositories\Sentinel\SentinelRoleRepository;

class Backup extends Command {

    protected $signature = 'mybackup:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Backup Notification And Run on Schedule ';

    /**
     * @var Notification
     */
    protected $notification;

    /**
     * Create a new command instance.
     *
     * @param Notification $notification
     */

    public function __construct(Notification $notification) {

        parent::__construct();
        $this->notification = $notification;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        Artisan::call("backup:run",["--only-db"=>true]);


        $jsonBody=json_encode([]);

        $this->notification->push(
            "newBackup_title",
            "newBackup_body$#$$jsonBody",
            'fas fa-hdd',
            $this->usersList(),
            'dashboard/backup/backup',
            "backup",
            null,
            null
        );
    }
    public function usersList(){
        $targetToPush=[];
        $st = \Setting::get('backup::Get-Notification');
        foreach (json_decode($st) as $usersRole){
            $role = new SentinelRoleRepository();
            $roleData = $role->find(intval($usersRole));

            foreach ($roleData->users as $target_user){
                array_push($targetToPush,$target_user->id);
            }
        }
        return $targetToPush;
    }

}
