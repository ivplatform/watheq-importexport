<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class IvSeeder extends Command {

    protected $signature = 'ivplatform:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Seed The Data Needed To Run ivplatform ';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $this->info('ivplatform seeders started...');
        $this->info('Seeding data');
        Artisan::call("db:seed",["--class"=>"Modules\Constant\Database\Seeders\ConstantDatabaseSeeder"]);
        Artisan::call("world:init");
        $this->info('Done!');
    }

}
