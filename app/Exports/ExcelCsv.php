<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ExcelCsv implements FromView
{
    protected $columns;
    protected $data;
    protected $file;
    public function __construct($columns,$data,$file)
    {
        $this->columns=$columns;
        $this->data=$data;
        $this->file=$file;
    }
    /**
     * @return View
     */
    public function view(): View
    {
        return view($this->file,["columns"=>$this->columns,"data"=>$this->data]);
    }
}
