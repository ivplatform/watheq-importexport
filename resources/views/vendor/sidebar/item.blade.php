
<li class="@if($item->getItemClass()){{ $item->getItemClass() }}@endif @if($active)active @endif @if($item->hasItems())treeview @endif clearfix">
    <a href="{{ $item->getUrl() }}" class="@if(count($appends) > 0) hasAppend @endif" @if($item->getNewTab())target="_blank"@endif>
        <i class="{{ $item->getIcon() }}"></i>
        <span>{{ $item->getName() }}</span>

        @foreach($badges as $badge)
            {!! $badge !!}
        @endforeach
    </a>

   <!-- @foreach($appends as $append)
        {!! $append !!}
    @endforeach -->

    @if(count($items) > 0)
        @if(strlen(implode($items))==0)
            <ul aria-expanded="false" class="collapse">
               <li>YOU DON'T HAVE PERMISSION</li>
            </ul>
        @else
        <ul aria-expanded="false" class="collapse">
            @foreach($items as $item)
                {!! $item !!}
            @endforeach
        </ul>
        @endif
    @endif
</li>
