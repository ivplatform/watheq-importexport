<?php

return [
    /*
    |--------------------------------------------------------------------------
    | The prefix that'll be used for the administration
    |--------------------------------------------------------------------------
    */
    'admin-prefix' => 'dashboard',

    /*
    |--------------------------------------------------------------------------
    | Location where your themes are located
    |--------------------------------------------------------------------------
    */
    'themes_path' => base_path() . '/Themes',

    /*
    |--------------------------------------------------------------------------
    | Which administration theme to use for the back end interface
    |--------------------------------------------------------------------------
    */
    'admin-theme' => 'Material',
    'change-front-theme' => false,

    /*
    |--------------------------------------------------------------------------
    | AdminLTE skin
    |--------------------------------------------------------------------------
    | You can customize the AdminLTE colors with this setting. The following
    | colors are available for you to use: skin-blue, skin-green,
    | skin-black, skin-purple, skin-red and skin-yellow.
    */
    'skin' => 'skin-blue',

    /*
    |--------------------------------------------------------------------------
    | WYSIWYG Backend Editor
    |--------------------------------------------------------------------------
    | Define which editor you would like to use for the backend wysiwygs.
    | These classes are event handlers, listening to EditorIsRendering
    | you can define your own handlers and use them here
    | Options:
    | - \Modules\Core\Events\Handlers\LoadCkEditor::class
    | - \Modules\Core\Events\Handlers\LoadSimpleMde::class
    */
    'wysiwyg-handler' => \Modules\Core\Events\Handlers\LoadCkEditor::class,
    /*
    |--------------------------------------------------------------------------
    | Custom CKeditor configuration file
    |--------------------------------------------------------------------------
    | Define a custom CKeditor configuration file to instead of the one
    | provided by default. This is useful if you wish to customise
    | the toolbar and other possible options.
    */
    'ckeditor-config-file-path' => '',

    /*
    |--------------------------------------------------------------------------
    | Middleware
    |--------------------------------------------------------------------------
    | You can customise the Middleware that should be loaded.
    | The localizationRedirect middleware is automatically loaded for both
    | Backend and Frontend routes.
    */
    'middleware' => [
        'backend' => [
            'auth.admin',
        ],
        'frontend' => [
        ],
        'api' => [
            'api',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Define which assets will be available through the asset manager
    |--------------------------------------------------------------------------
    | These assets are registered on the asset manager
    */
    'admin-assets' => [
        'bootstrap.css' => ['theme' => 'vendor/plugins/bootstrap/css/bootstrap.min.css'],
        'style.css'=>['theme' => 'css/style.css'],
        'rtl_style.css'=>['theme' => 'css/rtl_style.css'],
        'jquery.toast.css' => ['theme' => 'vendor/plugins/toast-master/css/jquery.toast.css'],
        'icheck.css' => ['theme' => 'vendor/plugins/icheck/skins/all.css'],
        'icheck.blue.css'=>['theme' => 'vendor/plugins/icheck/skins/all.css'],
        'bootstrap-select.min.css' => ['theme' => 'vendor/plugins/bootstrap-select/bootstrap-select.min.css'],
        'selectize.css' => ['module' => 'core:vendor/selectize/dist/css/selectize.css'],
        'selectize-default.css' => ['module' => 'core:vendor/selectize/dist/css/selectize.default.css'],
        'bootstrap.js' => ['theme' => 'vendor/plugins/bootstrap/js/bootstrap.min.js'],
        'jquery.js' => ['theme' => 'vendor/plugins/jquery/jquery.min.js'],
        'popper.js' => ['theme' => 'vendor/plugins/popper/popper.min.js'],
        'slimscroll.js' => ['theme' => 'js/jquery.slimscroll.js'],
        'waves.js' => ['theme' => 'js/waves.js'],
        'sidebarmenu.js' => ['theme' => 'js/sidebarmenu.js'],
        'sticky-kit-master.js' => ['theme' => 'vendor/plugins/sticky-kit-master/dist/sticky-kit.min.js'],
        'sparkline.js' => ['theme' => 'vendor/plugins/sparkline/jquery.sparkline.min.js'],
        'custom.js' => ['theme' => 'js/custom.min.js'],
        'ckeditor.js'=>['theme' => 'vendor/plugins/styleswitcher/jQuery.style.switcher.js'],
        'bootstrap-select.js' => ['theme' => 'vendor/plugins/bootstrap-select/bootstrap-select.min.js'],
        'jquery.toast.js' => ['theme' => 'vendor/plugins/toast-master/js/jquery.toast.js'],
        'toastr.js' => ['theme' => 'js/toastr.js'],
        'ckeditor.js' => ['theme' => 'vendor/plugins/ckeditor/ckeditor.js'],
        'icheck.js' => ['theme' => 'vendor/plugins/icheck/icheck.min.js'],
        'bootstrap-select.min.js' => ['theme' => 'vendor/plugins/bootstrap-select/bootstrap-select.min.js'],
        'selectize.js' => ['module' => 'core:vendor/selectize/dist/js/standalone/selectize.min.js'],

        // ==============================================================
        // calendars
        // ==============================================================
        'jquery.calendars.js'             =>  ['theme' => 'vendor/plugins/ummalqura/ummalqura/js/jquery.calendars.js'],
        'jquery.calendars-ar-EG.js'       =>  ['theme' => 'vendor/plugins/ummalqura/ummalqura/js/jquery.calendars-ar-EG.js'],
        'jquery.plugin.js'                =>  ['theme' => 'vendor/plugins/ummalqura/ummalqura/js/jquery.plugin.js'],
        'jquery.calendars.plus.js'        =>  ['theme' => 'vendor/plugins/ummalqura/ummalqura/js/jquery.calendars.plus.js'],
        'jquery.calendars.picker.js'      =>  ['theme' => 'vendor/plugins/ummalqura/ummalqura/js/jquery.calendars.picker.js'],
        'jquery.calendars.picker-ar.js'   =>  ['theme' => 'vendor/plugins/ummalqura/ummalqura/js/jquery.calendars.picker-ar.js'],
        'jquery.calendars.ummalqura.js'   =>  ['theme' => 'vendor/plugins/ummalqura/ummalqura/js/jquery.calendars.ummalqura.js'],
        'jquery.calendars.ummalqura-ar.js'=>  ['theme' => 'vendor/plugins/ummalqura/ummalqura/js/jquery.calendars.ummalqura-ar.js'],
        'calendar-convert.js'             =>  ['theme' => 'vendor/plugins/ummalqura/ummalqura/js/calendar-convert.js'],
        'ummalqura.calendars.picker.css' =>   ['theme' => 'vendor/plugins/ummalqura/ummalqura/css/ummalqura.calendars.picker.css'],

        // ==============================================================
        // dropzone
        // ==============================================================
        'bootstrap-wysihtml5.css'=>['theme' => 'vendor/plugins/html5-editor/bootstrap-wysihtml5.css'],
        'dropzone.css'=>['theme' => 'vendor/plugins/dropzone-master/dist/dropzone.css'],
        'wysihtml5-0.3.0.js'=>['theme' => 'vendor/plugins/html5-editor/wysihtml5-0.3.0.js'],
        'bootstrap-wysihtml5.js'=>['theme' => 'vendor/plugins/html5-editor/bootstrap-wysihtml5.js'],
        'dropzone.js'=>['theme' => 'vendor/plugins/dropzone-master/dist/dropzone.js'],
        'bootstrap-editable.css' => ['module' => 'translation:vendor/x-editable/dist/bootstrap3-editable/css/bootstrap-editable.css'],
        'bootstrap-editable.js' => ['module' => 'translation:vendor/x-editable/dist/bootstrap3-editable/js/bootstrap-editable.min.js'],

        // ==============================================================
        // select2
        // ==============================================================
        'select2.min.css' => ['theme' => 'vendor/plugins/select2-tree/select2.min.css'],
        'select2.min.js' => ['theme' => 'vendor/plugins/select2-tree/select2.min.js'],
        'select2totree.css' => ['theme' => 'vendor/plugins/select2-tree/select2totree.css'],
        'select2totree.js' => ['theme' => 'vendor/plugins/select2-tree/select2totree.js'],

        // ==============================================================
        // sweetalert
        // ==============================================================
        'sweetalert.min.js'=>['theme' => 'vendor/plugins/sweetalert/sweetalert.min.js'],
        'jquery.sweet-alert.custom.js'=>['theme' => 'vendor/plugins/sweetalert/jquery.sweet-alert.custom.js'],
        'sweetalert.css' => ['theme' => 'vendor/plugins/sweetalert/sweetalert.css'],

        // ==============================================================
        // datatable
        // ==============================================================
        'jquery.dataTables.js' => ['theme' => 'vendor/plugins/jquery-datatables-editable/jquery.dataTables.js'],
        'dataTables.bootstrap.js' => ['theme' => 'vendor/plugins/datatables/dataTables.bootstrap.js'],
        'select.dataTables.min.css'=>['theme' => 'vendor/plugins/datatables/select.dataTables.min.css'],
        'buttons.dataTables.min.css'=>['theme' => 'vendor/plugins/datatables/buttons.dataTables.min.css'],
        'dataTables.buttons.min.js'=>['theme' => 'vendor/plugins/datatables/dataTables.buttons.min.js'],
        'dataTables.export.js'=>['theme' => 'vendor/plugins/datatables/dataTables.export.js'],
        'jszip.min.js'=>['theme' => 'vendor/plugins/datatables/jszip.min.js'],
        'buttons.print.min.js'=>['theme' => 'vendor/plugins/datatables/buttons.print.min.js'],
        'buttons.html5.min.js'=>['theme' => 'vendor/plugins/datatables/buttons.html5.min.js'],
        'dataTables.select.min.js'=>['theme' => 'vendor/plugins/datatables/dataTables.select.min.js'],
        'vfs_fonts.js'=>['theme' => 'vendor/plugins/datatables/vfs_fonts.js'],
        'pdfmake.min.js'=>['theme' => 'vendor/plugins/datatables/pdfmake.min.js'],
        'DatatableScript.js'=> env('VERSION') == 'ARCHIVING' ? ['theme' => 'vendor/plugins/datatables/Arch_DatatableScript.js'] : ['theme' => 'vendor/plugins/datatables/DatatableScript.js'],
        'dataTables.colReorder.js' => ['theme' => 'vendor/plugins/datatables/dataTables.colReorder.min.js'],

        // ==============================================================
        // clockpicker
        // ==============================================================
        'moment-with-locales.min.js' =>   ['theme' => 'vendor/plugins/moment/min/moment-with-locales.min.js'],
        'jquery-clockpicker.min.js' =>   ['theme' => 'vendor/plugins/clockpicker/dist/jquery-clockpicker.min.js'],
        'jquery-clockpicker.min.css' =>   ['theme' => 'vendor/plugins/clockpicker/dist/jquery-clockpicker.min.css']
    ],

    /*
    |--------------------------------------------------------------------------
    | Define which default assets will always be included in your pages
    | through the asset pipeline
    |--------------------------------------------------------------------------
    */
    'admin-required-assets' => [
        'css' => [
            'bootstrap.css',
            'sweetalert.css',
            'jquery.toast.css',
            'style.css'
        ],
        'js' => [
            'popper.js',
            'bootstrap.js',
            'slimscroll.js',
            'sidebarmenu.js',
            'sticky-kit-master.js',
            'sparkline.js',
            'toastr.js',
            'jquery.toast.js',
            'sweetalert.min.js',
            'custom.js'
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Enable module view overrides at theme locations
    |--------------------------------------------------------------------------
    | By default you can only have module views in resources/views/asgard/[module]
    | setting this setting to true will add ability for you to store those views
    | in any of front or backend themes in my-theme/views/modules/[module]/...
    |
    | useViewNamespaces.backend-theme needs to be enabled at module level
    */
    'enable-theme-overrides' => true,
];
